#!/bin/bash
# Copyright Huawei Technologies Co.. Ltd. 2020-2021. All rights reserved.

set -e

LOG_TAG="Wifi_mt"
LOG_NAME="${0}:"

logi ()
{
    /system/bin/log -t $LOG_TAG -p i "$LOG_NAME $@"
}

echo stop > /proc/wifi_built_in/wifi_start
if [ "$?" == "0" ]; then
    logi "rmmod wlan success"
else
    logi "rmmod wlan fail."
fi


